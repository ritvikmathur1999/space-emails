import smtplib
import csv
import requests
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import sys

print("Up and running: Mr "+sys.argv[5])


def Users(email,subject_in,sender,password,message):
  subject = subject_in
  msg = MIMEMultipart()
  email_user = sender #CHANGE email_address to FROM EMAIL ADDRESS
  email_password = password #CHANGE password to FROM PASSWORD
  email_send = email # DO NOT MAKE CHANGES TO THIS
  msg['From'] = email_user
  # converting list of recipients into comma separated string
  msg['To'] = ", ".join(email_send)
  msg['Subject'] = subject
  body = message
  msg.attach(MIMEText(body, 'html'))
  text = msg.as_string()
  server = smtplib.SMTP('smtp.gmail.com',587)
  server.starttls()
  server.login(email_user,email_password)
  server.sendmail(email_user,email_send,text)
  server.quit()

sub=sys.argv[1]
sen=sys.argv[2]
passw=sys.argv[3]
msg=sys.argv[4]
file_name=sys.argv[5]



results = []
with open("public\\"+file_name, newline='') as inputfile: # CHANGE CSV FILE NAME
    for row in csv.reader(inputfile):
        results.append(row[0])
for i in range(len(results)):
    Users(list(results[i].split(" ")),sub,sen,passw,msg)
