var Router = require('./router/router.js') 
var https=require('https')
const express = require('express')
const {spawn} = require('child_process');
const app = express()
const cors= require("cors")
var multer = require('multer')
const bodyParser = require('body-parser');
const { nextTick } = require('process');
const fs = require('fs')
app.use(express.json())
app.use(cors())
app.use(bodyParser.json({limit: '50mb'})) // for parsing application/json
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }))

const PORT = 8000
const HOST='0.0.0.0';
var httpsServer = https.createServer({},app);
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
    cb(null, 'public')
  
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' +file.originalname )
  }
}) 

var upload = multer({ storage: storage }).single('file')


var file_name=null;

app.post("/upload",(req,res)=>{
    upload(req, res, function (err) {
        res.setHeader("Content-Type", "text/html");
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        file_name=req.file.filename

        // console.log(file_name)
   return res.status(200).send(req.file)


    })
});

app.get('/',Router);

app.post("/send", (req,res,next)=>{
    
    var dataToSend;
    // spawn new child process to call the python script
    const python = spawn('python', ['./script/mb.py',req.body.subject,req.body.sender,req.body.password,req.body.message,file_name]);
   
    // collect data from script
    python.stdout.on('data', function (data) {
     console.log('Pipe data from python script ...');
     dataToSend = data.toString();
    });
    // in close event we are sure that stream from child process is closed
    python.on('close', (code) => {
    console.log(`child process close all stdio with code ${code}`);
    console.log(req.body)
    return res.redirect('/home');
    });
    
})


app.get('/home',(req,res)=>{
    res.send("Email has been sent!")
})

https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }, app).listen((PORT), () => {
    console.log(`Listening to port ${PORT}`)
  })

// app.listen(PORT, HOST, () => console.log(`http://localhost:${PORT}`))